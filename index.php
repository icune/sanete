<?php 
	session_start();
	require_once('s/inc/core/config.php');

	require_once('s/inc/core/check.php');
	require_once('s/inc/core/view.php');
	require_once('s/inc/core/db.php');
	require_once('s/inc/core/info.php');
	require_once('s/inc/core/utils.php');
	require_once('s/inc/core/gui.php');
	require_once('s/inc/core/user.php');
	require_once('s/inc/core/lang.php');
	require_once('s/inc/core/basket.php');
	require_once('s/inc/out.php');
	require_once('s/inc/post_controller.php');
	require_once('s/inc/get_controller.php');
	require_once('s/inc/route.php');
	
	

	try{
		route();
	}catch(Exception $e){
		echo "[ERROR] ".$e->getMessage()."\n";
		print_r($e->getTrace());
	}

?>
