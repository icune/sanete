<?php 

final class Out{
	public static function json($o){
		header('Content-Type: application/json');
		echo json_encode($o);
		exit();
	}
	public static function error($msg = null){
		self::json(array('status' => 'error', 'msg' => $msg));
	}
	public static function ok($msg = null){
		self::json(array('status' => 'ok', 'msg' => $msg));
	}
}