<?php 
class PostController{
	public function __construct(){

		if(!isset($_POST['act']) || !preg_match('![a-z_]+!' ,$_POST['act']))
			Out::error('act not set');

		if(!method_exists($this, $_POST['act']))
			Out::error("${_POST['act']} absent");

		$this->{$_POST['act']}();
	}
}