<?php
function route(){
	$script = Info::$script;
	if(!strlen($script))
		$script = 'main';
	$fn = Info::$rootDir . '/s/' . $script .'.php';
	if(file_exists($fn)){
		require_once($fn);
		exit();
	}else{
		require_once(Info::$rootDir . '/s/404.php');
		exit();
	}

}