<?php
final class Db{
	public static $connectionId = null;
	public static function init(){
		error_reporting(E_ALL ^ E_DEPRECATED);
		self::$connectionId = mysql_connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASSWORD);
		mysql_select_db(Config::DB_DB);
		mysql_query ("set character_set_client='utf8'"); 
		mysql_query ("set character_set_results='utf8'"); 
		mysql_query ("set collation_connection='utf8_general_ci'");
	}
	protected static function replaceParam($sql, $type, $param, $value){
		if($type == 's' || $type == 'd')
			$value = '"'.mysql_real_escape_string($value).'"';
		else if($type == 'i' || $type == 'f')
			$value = mysql_real_escape_string($value);
		else if($type == 'c')
			$value = '`'.mysql_real_escape_string($value).'`';
		return str_replace($type.':'.$param, $value, $sql);
	}
	public static function sp($s, $p = array()){
		$types = array('s', 'i', 'f', 'd', 'c');
		foreach($p as $pn=>$pv){
			foreach($types as $t)
				while(strpos($s, $t.':'.$pn) !== FALSE){
					$s = self::replaceParam($s, $t, $pn, $pv);
				}
		}
		return $s;
	}
	public static function q($sql, $params = array()){
		$sql = self::sp($sql, $params);
		file_put_contents(Info::$rootDir . '/' . 'log.sql', '['.date('H:i:s').'] '.$sql."\n\n", FILE_APPEND);
		return self::fetchSql($sql);
	}
	public static function fetchSql($sql){
		$r = array();
		$qr = mysql_query($sql);
		if(is_bool($qr))
			return $qr;
		while($row = mysql_fetch_assoc($qr))
			$r []= $row;
		return $r;
	}
	public static function id(){
		return mysql_insert_id();
	}
	
}
Db::init();