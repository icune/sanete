<?php 
	final class User{
		public static $logged = false;
		public static $login, $id, $hash;
		private static $salt = 'qur5music0nur';
		private static $cookieName = 'ckcounter';
		public static function init(){
			self::logged();
		}
		public static function register($login, $password, $uncookie = false){
			$was_id = self::$id;

			$sql = 'INSERT INTO user(login, password) ';
			$sql .= 'VALUES (s:login,s:password)';
			$hash = self::uniqueHash();
			Db::q($sql, array(
				'login' => $login,
				'password' => self::passwordHash($password)
			));
			if(!$uncookie)
				setcookie(self::$cookieName, $hash);

			self::$id = Db::id();
			self::$login = $login;
			return self::$id;
		}
	
		public static function logged(){
			self::$logged = self::getUser() ? true : false;
			return self::$logged;
		}
		public static function getUser(){
			$hash = isset($_COOKIE[self::$cookieName]) ? $_COOKIE[self::$cookieName] : null;
			if(!$hash)
				return null;
			$hashed = self::getByHash($hash);
			if(!$hashed)
				return null;

			self::$login = $hashed['login'];
			self::$id = $hashed['id'];
			return $hashed;
		}
		public static function log($login, $password){
			$was_id = self::$id;
			$users = Db::q('SELECT * FROM user WHERE login=s:login AND password=s:password', array(
				'login' => $login,
				'password' => self::passwordHash($password)
			));
			if(!count($users))
				return false;
			$user = $users[0];
			$hash = self::uniqueHash();
			Db::q('UPDATE user SET hash=s:hash WHERE id=i:id', array('hash'=>$hash, 'id'=>$user['id']));
			$user['hash'] = $hash;
			setcookie(self::$cookieName, $hash);

			self::$login = $user['login'];
			self::$id = $user['id'];

			$user['cookie_js'] = self::$cookieName.'='.$user['hash'];
			return $user;
		}
		public static function logout(){
			setcookie(self::$cookieName, null);			
		}
		public static function getByHash($hash){
			$r = Db::q('SELECT * FROM user WHERE hash=s:hash', array('hash' => $hash));
			if(!count($r))
				return null;
			return $r[0];
		}
		public static function getByLogin($login){
			$r =  Db::q('SELECT * FROM user WHERE login=s:login', array('login' => trim($login)));	
			if(!count($r))
				return null;
			return $r[0];
		}
		public static function getById($id){
			$r = Db::q('SELECT * FROM user WHERE id=i:id', array('id' => $id));	
			if(!count($r))
				return null;
			return $r[0];
		}
		private static function uniqueHash(){
			$hash = Utils::rStr(40);
			return $hash;
		}
		public static function passwordHash($password){
			return crypt($password, self::$salt);
		}

		public static function setHash($hash){
			setcookie(self::$cookieName, $hash);
		}
	}
User::init();