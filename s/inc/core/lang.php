<?php 
	final class Lang{
		public static $code = 'en';
		public static $codes = array('en', 'ru');
		public static $dict = null;
		public static function init(){
			if(isset($_GET['language_code'])){
				$_SESSION['language_code'] = $_GET['language_code'];
			};
			self::$code = isset($_SESSION['language_code']) ? $_SESSION['language_code'] : self::$code;
			self::get();
		}
		public static function get(){
			if(self::$dict) return self::$dict;
			$lang_file = Info::$rootDir.'/s/lang/'.self::$code.'.php';
			require($lang_file);
			self::$dict = $_;
			return $_;
		}
	}
Lang::init();