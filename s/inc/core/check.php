<?php 
final class Check{
	public static function  postAbsent($field){
		if(!isset($_POST[$field]))
			Out::error("$field absent");
		return $_POST[$field];
	}
	public static function  getAbsent($field){
		if(!isset($_GET[$field]))
			Out::error("$field absent");
		return $_GET[$field];
	}
}