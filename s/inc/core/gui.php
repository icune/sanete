<?php 
	final class Gui{
		public static function head(){
			self::view('head.php');
		}
		public static function tail(){
			self::view('tail.php');
		}
		public static function menu($data = array()){
			self::view('menu.php', $data);
		}
		public static function view($fileName, $data = array(), $ret = false){
			$fileName = Info::$rootDir.'/s/view/'.$fileName;
			$data['_USER'] = array(
				'logged' => User::$logged,
				'id' => User::$id,
				'login' => User::$login
			);
			$data['lang'] = Lang::get();
			extract($data);
			ob_start();
			require($fileName);
			$output = ob_get_contents();
			ob_end_clean();
			if($ret)
				return $output;
			echo $output;
		}
	}