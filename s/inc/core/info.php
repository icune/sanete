<?php
final class Info{
	public static $sid = null, $rootDir = null, $printNumber = null;
	public static $uri = null, $query = null;
	public static $isWin = false, $isNix = false;
	public static $script = '';
	public static $host = '';
	public static function init(){

		self::$rootDir = rtrim($_SERVER['DOCUMENT_ROOT'], '/');
		self::$uri = $_SERVER['REQUEST_URI'];
		self::$query = $_SERVER['QUERY_STRING'];
		self::$host = $_SERVER['HTTP_HOST'];
		preg_match('!/([a-zA-Z-]+)[?/]?!',  self::$uri, $mtc);
		if($mtc && isset($mtc[1]))
			self::$script =  $mtc[1];

		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		    self::$isWin = true;
		} else {
		    self::$isNix = true;
		}
	}
	public static function sessionPages(){
		$pages = array();
		$sid = self::$sid;
		$files = scandir(self::$rootDir."/pages/$sid");
		foreach($files as $file){
			if(is_file(self::$rootDir."/pages/$sid".'/'.$file))
				$pages []= file_get_contents(self::$rootDir."/pages/$sid".'/'.$file);
		};
		return $pages;
	}
	public static function sessionImages(){
		$images = array();
		if(isset($_SESSION['upload']))
			$images = $_SESSION['upload'];
		return $images;
	}
	public static function getUri($get = null){
		if(!$get)
			return self::$uri;
		$get_arr = array();
		if(is_string($get))
			$get_arr = array($get);
		else
			$get_arr = $get;
		if(strpos(Info::$uri, 'deusiuuare') !== false)
			$get_arr []= 'deusiuuare';
		$uri = preg_replace('!\?.*$!', '', Info::$uri);
		return $uri.'?'.implode('&', $get_arr);

	}
}

Info::init();