<?php 

	final class Basket{
		public static $count = 0;
		public static $id = 0;
		public static $cookie='basket';
		public static function init(){
			if(isset($_POST['basket_idsalt'])){
				self::$id = $_POST['basket_idsalt'];
				return;
			}
			if(self::cookied())
				self::$id = $_COOKIE[self::$cookie];
			else{
				self::$id = self::create();
				setcookie(self::$cookie, self::$id, time()+60*60*24*30, '/');
			}
		}
		public static function cookied(){
			if(!isset($_COOKIE[self::$cookie]))
				return false;
			if(!self::check($_COOKIE[self::$cookie]))
				return false;
			return true;
		}
		public static function update($product_id, $count){
			if(!self::$id)
				return false;
			Db::q('UPDATE basket_item SET count=i:count WHERE product_id=i:product_id AND basket_id=i:basket_id', array(
				'basket_id' => self::$id,
				'product_id' => $product_id,
				'count' => $count
			));
		}
		public static function add($product_id, $count){
			if(!self::$id)
				return false;
			Db::q('INSERT INTO basket_item(basket_id, product_id, count) VALUES(i:basket_id, i:product_id, i:count)', array(
				'basket_id' => self::$id,
				'product_id' => $product_id,
				'count' => $count
			));
		}
		public static function delete($product_id){
			if(!self::$id)
				return false;
			Db::q('DELETE FROM basket_item WHERE product_id=i:product_id AND basket_id=i:basket_id', array(
				'basket_id' => self::$id,
				'product_id' => $product_id
			));
		}
		public static function create(){
			Db::q('INSERT INTO basket VALUES();');
			self::$id = Db::id();
			return self::$id;
		}
		public static function order($data){
			if(!self::$id)
				return false;
			$req_fields = array('contacts', 'comment', 'current');
			foreach($req_fields as $rf) if(!isset($data[$rf])) return false;
			Db::q('UPDATE basket SET order_ts=NOW(), ordered = 1, contacts=s:contacts, comment=s:comment WHERE id=i:id', array(
				'contacts' => $data['contacts'],
				'comment' => $data['comment'],
				'id' => self::$id
			));
			Db::q('DELETE FROM basket_item WHERE basket_id=i:id', array('id' => self::$id));

			$current = $_POST['current'];

			$vals = array();
			foreach($current as $cur)
				$vals []= Db::sp('(i:basket_id, i:product_id, i:count)', array(
					'basket_id' => self::$id,
					'product_id' => $cur['product_id'],
					'count' => $cur['count'],
				));
			Db::q('INSERT INTO basket_item(basket_id, product_id, count) VALUES '.implode(',', $vals));
			
			setcookie(self::$cookie, null);

			return true;
		}
		public static function get(){
			return Db::q('SELECT * FROM basket_item bi JOIN product p ON p.id = bi.product_id WHERE basket_id=i:id', array('id' => self::$id));
		}
		private static function check($id){
			return count(Db::q('SELECT * FROM basket WHERE id=i:id', array('id' => $id)));
		}
		public static function state($val){
			Db::q('UPDATE basket SET processed=i:p WHERE id=i:id', array(
				'p' => $val,
				'id' => self::$id
			));
		}
	}