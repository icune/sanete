<?php 
	final class Utils{
		public static function rStr($cnt, $set = null){
			if(!$set)
				$set = array(array('a', 'z'), array('A', 'Z'), array('0', '9'));
			$chMap = array();
			foreach($set as $s)
				for($code = ord($s[0]); $code <= ord($s[1]); $code++)
					$chMap []= chr($code);
			$r = '';
			for($i = 0; $i < $cnt; $i++)
				$r .= $chMap[rand()%count($chMap)];
			return $r;
		}
		public static function postTo($domain, $post){
		//Проверка на правильность URL 
			if(!filter_var($domain, FILTER_VALIDATE_URL)){
				throw new Exception("CURL: Bad $domain domain!");
				return false;
			}

			//Инициализация curl
			$curlInit = curl_init($domain);
			curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
			curl_setopt($curlInit,CURLOPT_HEADER,false);
			curl_setopt($curlInit,CURLOPT_NOBODY,false);
			curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

			curl_setopt($curlInit, CURLOPT_POST, true);
			curl_setopt($curlInit, CURLOPT_POSTFIELDS, $post);
			//Получаем ответ
			$response = curl_exec($curlInit);

			curl_close($curlInit);

			return $response;
		}
		public static function simple($arr){
			//array(array(xxx=>1), array(xxx=>2)) => array(1, 2)
			$r = array();
			if(!count($arr)) return $r;
			$keys = array_keys($arr[0]);
			$key = $keys[0];
			foreach($arr as $arr_el){
				$r []= $arr_el[$key]; 
			}
			return $r;
		}
	}