<?php 
	class BasketController extends PostController{
		
		public function __construct(){
			Basket::init();
			parent::__construct();
		}
		public function update(){
			Check::postAbsent('product_id');
			Check::postAbsent('count');
			Basket::update($_POST['product_id'], $_POST['count']);
			Out::ok();
		}
		public function add(){
			Check::postAbsent('product_id');
			Check::postAbsent('count');
			Basket::add($_POST['product_id'], $_POST['count']);
			Out::ok();
		}
		public function delete(){
			Check::postAbsent('product_id');
			Basket::delete($_POST['product_id']);
			Out::ok();
		}
		public function order(){
			Check::postAbsent('contacts');
			Check::postAbsent('comment');
			Check::postAbsent('current');
			Basket::order($_POST);
			Out::ok();
		}
		public function get(){
			Out::ok(Basket::get());
		}
		public function state(){
			Check::postAbsent('val');
			Basket::state($_POST['val']);
			Out::ok();
		}
		public function our_comment(){
			$id = Check::postAbsent('id');
			$val = Check::postAbsent('val');
			Db::q('UPDATE basket SET our_comment=s:c WHERE id=i:id',  array(
				'c' => $val,
				'id' => $id
			));
			Out::ok();
		}
	}
	new BasketController();