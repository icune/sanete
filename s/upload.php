<?php 
class Upload extends PostController{
	public function upload(){
		$file = $_FILES['file'];
		if($file['error'])
			Out::error('file error');
		$uploadPath = Info::$rootDir . '/img/photo/';
		if(!is_dir($uploadPath))
			mkdir($uploadPath);
		$ext = pathinfo($file['name']);
		$ext = $ext['extension'];
		if(!preg_match('!jpg|jpeg|png|gif|bmp!', $ext))
			Out::error('wrong extension');
		$ext=".$ext";
		do{
			$new_name = Utils::rStr(20) . $ext;
		}while(file_exists($uploadPath.$new_name));
		
		copy($file['tmp_name'], $uploadPath.$new_name);
		User::update(null, null, null, null, '/img/photo/'.$new_name);
		Out::json(array(
			'status' => 'ok',
			'file' => '/img/photo/'.$new_name
		));
	}
}
	
new Upload();
	