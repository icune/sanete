<?php Basket::init(); ?>
<div class="menu shadow1">
	<div class="menu-center">
		<a class="menu-item logo" href="/">
			<div class="catalog-icon menu-item-icon"></div>
			<span class="menu-item-text">
				
			</span>
		</a>
		<div class="spacer"></div>
		<a class="catalog menu-item hoverable <?php if(Info::$script=='catalog') echo 'active'; ?>" href="/catalog">
			<div class="catalog-icon menu-item-icon"></div>
			<span class="menu-item-text">
				Каталог
			</span>
		</a>
		<?php if(Info::$script=='catalog' || Info::$script=='plist'): ?>
			<a id="cats" class="catalog menu-item hoverable" style="background-color: #6BC86B;color: #444444;">
				<div class="catalog-icon menu-item-icon"></div>
				<span class="menu-item-text">
					Категории <span class="count"></span>
				</span>
			</a>
		<?php endif; ?>
		<a class="contact menu-item hoverable <?php if(Info::$script=='contact') echo 'active'; ?>" href="/contact">
			<div class="contact-icon menu-item-icon"></div>
			<span class="menu-item-text">
				Контакты
			</span>
		</a>
		<?php if(User::$logged): ?>
		<a class="list menu-item hoverable <?php if(Info::$script=='plist') echo 'active'; ?>" href="/plist">
			<div class="catalog-icon menu-item-icon"></div>
			<span class="menu-item-text">
				Список
			</span>
		</a>
		<a class="orders menu-item hoverable <?php if(Info::$script=='orders') echo 'active'; ?>" href="/orders">
			<div class="catalog-icon menu-item-icon"></div>
			<span class="menu-item-text">
				Заказы
			</span>
		</a>
		<a class="user-info menu-item">
			<div class="catalog-icon menu-item-icon"></div>
			<span class="menu-item-text">
				Пользователь <b><?php echo User::$login ?></b>,
				<span id="logout" href="/logout" style="display:inline;text-decoration:underline">Выйти</span>
				<script type="text/javascript">
					$(function(){
						$('#logout').click(function(){
							window.location.href='/logout';
						})
					})
				</script>
			</span>
		</a>
	<?php endif; ?>
		<div class="basket menu-item hoverable">
			<span class="menu-item-text">
				Корзина: <span class="count"><?php 	echo count(Basket::get()); ?></span>
				<script>
					Basket.data.count = <?php 	echo count(Basket::get()); ?>;
				</script>
			</span>
			<div class="menu-item-icon"></div>
		</div>
	</div>
</div>
<div class="page-menu">
	<div class="head">
		
		<h3>
				Выбор категорий 
		</h3>
		<div class="cross"></div>
		<div class="buttons">
			<div class="button bottom-border" id="reset" style="display:none">
				Сбросить
			</div>
			<div class="button" id="show" style="display:none">
				Показать
			</div>
			
		</div>
	</div>
	
	<div class="container">
		
		<table>
			<?php 
				$cols = 4;
				$rows = intval(count($cats)/$cols)+1;
				$mtx = array();
				for($i = 0; $i < $rows; $i++){
					$row = array();
					for($j = 0; $j < $cols; $j++){
						$row []= array('name' => '', 'count' => 0);
					};
					$mtx []= $row;
				};
				$inx = 0;
				for($i = 0; $i < $cols; $i++){
					for($j = 0; $j < $rows; $j++){
						if($inx < count($cats))
							$mtx[$j][$i] = $cats[$inx++];
						else
							break;
					};					
					if($inx >= count($cats)) break;
				};
				for($i = 0; $i < $rows; $i++){
					echo '<tr>';
					for($j = 0; $j < $cols; $j++){
						if(trim($mtx[$i][$j]['name']))
							echo '<td class="category"><span class="name">' . 
								$mtx[$i][$j]['name'] . 
								'</span>' . 
								' (' . 
								$mtx[$i][$j]['count'] . 
								')' . 
								'<div class="plus">' .
								'</div></td>';
					};
					echo '</tr>';
				};

			 ?>
		</table>
	</div>
</div>