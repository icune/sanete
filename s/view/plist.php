<?php echo Gui::head(); ?>
<?php echo Gui::menu(array(
	'cats' => $cats
)); ?>
<link rel="stylesheet" href="/plug/no-ui-slider/nouislider.min.css">
<script type="text/javascript" src="/js/filter.js"></script>
<script type="text/javascript" src="/js/plist.js"></script>
<link rel="stylesheet" type="text/css" href="/css/filter.css">
<link rel="stylesheet" type="text/css" href="/css/plist.css">
<script type="text/javascript" src="/plug/no-ui-slider/nouislider.min.js"></script>

<div class="content">
	<?php Gui::view('filter.php') ?>
	<div class="edit-toolbar gradient1 shadow1">
		<input type="checkbox" id="show-changed">
		<label for="show-changed">Показывать измененные</label>
		<input type="checkbox" id="show-filter" checked="checked">
		<label for="show-filter">Показывать фильтр</label>

		<button id="save">Сохранить</button>
	</div>
	<div class="changed-products">
		
	</div>
	<div class="products">
		
	</div>
	<div id="block-end"></div>
	<div class="nothing-found shadow1 gradient1">
		Ничего не найдено
	</div>
	<img src="/img/loading.gif" alt="" class="loader" id="product-loader">

	<div class="product shadow1 gradient1 template">
		<div class="fields struct"></div>
		<button class="delete">
			Удалить
		</button>
	</div>

	<script type="text/javascript">
		(function(){
			var f = {
				<?php 
					$fields = Db::q("SHOW COLUMNS FROM product");
					$ff = array();
					foreach($fields as $f)
						$ff []= '"' . $f['Field'] . '":"' . $f['Type'] . '"';
					echo implode(",\n", $ff);
				 ?>
			}
			var fn = {
				'img' : "Изображение",
				'code' : "Артикул",
				'name' : "Наименование",
				'price' : "Цена",
				'weight' : "Вес",
				'gost' : "ГОСТ",
				'size_x' : "Длина",
				'size_y' : "Ширина",
				'size_z' : "Глубина",
				'cnt' : "Количество",
				'description' : "Описание",
				'new' : "Новый",
				'cat' : "Категория",
				'manf' : "Производитель",
				'act' : "Активен",
				'tolink': "\\",
				'description_seo' : "\\СЕО описание",
				'var' : "\\Вариант",
				'rec' : "\\",
				'sort' : "\\",
				'link' : "\\",
				'nameseo' : "\\СЕО имя",
				'oldprice' : "\\Старая цена",
				'url' : "\\",
				'kwdseo' : "\\СЕО кворды"
			}
			var rf = {}
			for(var k in fn)
				if(f[k])
					rf[k] = {type:f[k], name:fn[k], field:k};
			Globals.product_field = rf;
		})();

		Globals.product_changed = {};
	</script>
	
	<div class="to-top">Наверх</div>
</div>
<?php echo Gui::tail(); ?>