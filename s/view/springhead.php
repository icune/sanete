<?php Gui::head(); ?>
<?php Gui::menu(); ?>
<script type="text/javascript" src="/js/net.js"></script>
<script type="text/javascript" src="/js/login.js"></script>

<style type="text/css">
	.login-box{
		width:300px;
		margin:0 auto;
	}
	.login-box input,button{
		width:100%;
		padding:5px;
		box-sizing:border-box;
		margin-bottom:5px;
	}
	.login-box input.wrong{
		background-color:rgb(255, 163, 163);
	}

</style>
<div class="content shadow1 text">
	<div class="login-box">
		<input type="text" id="login" placeholder="Логин"/>
		<input type="password" id="password" placeholder="Пароль"/>
		<button class="enter">Да</button>
	</div>
</div>
<?php Gui::tail(); ?>