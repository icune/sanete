
<?php echo Gui::head(); ?>
<?php echo Gui::menu(array('cats' => $cats)); ?>

	<link rel="stylesheet" href="/plug/no-ui-slider/nouislider.min.css">
	<link rel="stylesheet" type="text/css" href="/css/catalog.css">
	<link rel="stylesheet" type="text/css" href="/css/filter.css">
	<script type="text/javascript" src="/js/filter.js"></script>
	<script type="text/javascript" src="/js/catalog.js"></script>
	<script type="text/javascript" src="/plug/no-ui-slider/nouislider.min.js"></script>

	<div class="content">
		<?php Gui::view('filter.php'); ?>

		<div class="blocks"></div>
		<div id="block-end"></div>
		<div class="nothing-found shadow1 gradient1">
			Ничего не найдено
		</div>
		<img src="/img/loading.gif" alt="" class="loader" id="product-loader">
	</div>
	<div class="to-top shadow" style="display:none">
		Наверх
	</div>
<?php echo Gui::tail(); ?>