<?php echo Gui::head(); ?>
<?php echo Gui::menu(); ?>
	<div class="content text shadow1">
		<h2>Добро пожаловать!</h2><br /><br />
		<p>В течение многих лет ведущие мировые производители сантехники и мебели для ванной комнаты совершенствуют свою продукцию, чтобы клиент мог создать себе теплый и уютный дом.</p>
		<br />
		<p>Благодаря профессионализму наших специалистов мы в течении 10 лет помогаем покупателям, создавая уникальные экспозиции и собирая самые качественные, оригинальные и популярные бренды со всего мира.</p>
		<br />
		<p>За время своего существования компания по праву заняла одно из ведущих мест на российском рынке товаров для ванных комнат и жилых помещений. Среди наших постоянных заказчиков - специалисты в области строительства и оформления интерьеров: архитекторы, дизайнеры-профессионалы, которые доверяют нашим знаниям, умениям и опыту. Круг наших клиентов расширяется день ото дня. И тому есть веские причины. Мы стремимся предоставить нашему покупателю комплекс товаров и услуг высшего качества.</p>
		<br>
		<b>
			В данный момент идет переиндексация цен. Поэтому уточняйте цену, отправив письмо на 
			<a href="mailto:link@sanete.ru">link@sanete.ru</a>
		</b>
	</div>
<?php echo Gui::tail(); ?>