<div class="filter shadow1">
	<table class="cat-table">
		<tr>
			<!-- <th>Категории</th> -->
			<th>Названия</th>
			<th>Артикулы</th>
			<th>Цена, руб.</th>
			<th>Вес, кг</th>
			<th>Размеры, мм</th>
			<th>Производители</th>
		</tr>
		<tr>
			<!-- <td id="cat-filter"></td> -->
			<td id="names-filter"></td>
			<td id="codes-filter"></td>
			<td id="price-filter"></td>
			<td id="weight-filter"></td>
			<td id="size-filter"></td>
			<td id="manf-filter"></td>
		</tr>
		<tr class="row-with-reset">
			<td colspan="5" class="found"></td>
			<td >
				<button class="reset-button" style="display:none">Сбросить</button>
			</td>		
		</tr>
	</table>
	<img src="/img/loading.gif" alt="" id="filter-loader" class="loader">

</div>