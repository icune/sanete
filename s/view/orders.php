<?php echo Gui::head(); ?>
<?php echo Gui::menu(); ?>
<style type="text/css">
	.orders-table{
		width:100%;
	}
	.orders-table th{
		padding-bottom:20px;
	}
	.orders-table td{
		padding-bottom:5px;
	}
	.show-basket{
		text-decoration: underline;
		cursor: pointer;
	}
	button.processed{
		width:150px;
	}
	button.processed.active .ui-button-text{
		background-color: rgb(142, 231, 142);
	}
</style>
<script type="text/javascript" src="/js/basket-admin.js"></script>
<script type="text/javascript">
	function showBasket(id){
		BasketAdmin.open(id);
	}
	$(function(){
		$('.modal-basket .fields').remove();
		$('button.processed').button().click(function(){
			$(this).toggleClass('active');
			if($(this).find('span').text().trim() == 'Завершить')
				$(this).find('span').text('Возобновить');
			else
				$(this).find('span').text('Завершить');
			Net.post('basket', 'state', {
				val:!$(this).hasClass('active'), 
				basket_idsalt:$(this).attr('basket_id')
			});
		});
		$('.our_comment').keyup(function(){
			Net.post('basket', 'our_comment', {
				val:$(this).val(), 
				id:$(this).attr('basket_id')
			});
		})
	})
</script>
<div class="content shadow1 text">
	<h3>
		Заказы
	</h3>
	<br><br>
	<?php 
		$orders = Db::q('SELECT * FROM basket WHERE ordered ORDER BY order_ts DESC');
	?>
	<table class="orders-table">
		<tr>
			<th>
				Контакты
			</th>
			<th>
				Комментарий клиента
			</th>
			<th>
				Время заказа
			</th>
			<th>
				Корзина
			</th>
			<th>
				Наш комментарий
			</th>
			<th>
				Завершен
			</th>
		</tr>
		<?php foreach($orders as $o): ?>
			<tr>
				<td>
					<?php echo $o['contacts'] ?>
				</td>
				<td>
					<?php echo $o['comment'] ?>
				</td>
				<td>
					<?php echo $o['order_ts'] ?>
				</td>
				<td>
					<div class="show-basket" onclick="showBasket(<?php echo $o['id'];?>); return false;">Показать корзину</div>
				</td>
				<td>
					<textarea  basket_id="<?php echo $o['id']; ?>" class="our_comment"><?php echo $o['our_comment'] ?></textarea>
				</td>
				<td>
					<button basket_id="<?php echo $o['id']; ?>"class="processed <?php if(!$o['processed']) echo 'active'; ?>">
						<?php if($o['processed']) echo 'Возобновить'; else echo 'Завершить'; ?>
					</button>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<?php echo Gui::tail(); ?>