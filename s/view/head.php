<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="/css/font.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
	<link rel="stylesheet" type="text/css" href="/css/block.css">
	
	
	<script src="/js/jquery-2.1.3.js"></script>

	<script type="text/javascript" src="/plug/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/plug/jquery-ui-1.11.4.custom/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="/plug/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css">
	<link rel="stylesheet" type="text/css" href="/plug/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css">

	<script src="/js/smooth-scroll.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/ui.js"></script>
	<script type="text/javascript" src="/js/net.js"></script>

	<script type="text/javascript" src="/js/basket.js"></script>
	<script type="text/javascript">
		window.Globals = {num:0, filters:{}, products:{}};
	</script>
</head>
<body>
		
		<?php Gui::view('block.php'); ?>
		<?php Gui::view('basket.php'); ?>