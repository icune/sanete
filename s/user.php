<?php 

class ActionsUser extends PostController{
	public function login(){
		$login = Check::postAbsent('login');
		$password = Check::postAbsent('password');


		$logged = User::log($login, $password);
		if($logged)
			Out::ok($logged['cookie_js']);
		else
			Out::error('no user with such pair');
	}
	public function logout(){
		User::logout();
		Out::ok('ok');
	}
	public function register(){
		$login = Check::postAbsent('login');
		$email = Check::postAbsent('email');	
		if(isset($_POST['boss']) && $_POST['boss'] == 'deusiuuare'){
			$inviter_id = 0;
		}else{
			$inviter_id = intval(Check::postAbsent('inviter_id'));
			if(!$inviter_id)
				Out::error();
		}
		// Сохранено поржать
		// $inviter_id = isset($_POST['inviter_id']) ? intval($_POST['inviter_id']) : 0;
		// if(!$inviter_id && isset($_POST['boss']) && $_POST['boss'] == 'deusiuuare')
		// 	$inviter_id = 0;
		// else	
		// 	$inviter_id = intval(Check::postAbsent('inviter_id'));

		if(User::getByLogin($login))
			Out::error('afore-login');
		if(User::getByEmail($email))
			Out::error('afore-email');
		if(trim($perfect) && User::getByPerfect($perfect))
			Out::error('afore-perfect');
		$newId = User::register($login, $password, $email, $perfect, isset($_POST['photo']) ? $_POST['photo'] : '', $inviter_id, isset($_POST['uncookie']));

		Out::ok($newId);
	}
	public function update(){
		$perfect = Check::postAbsent('perfect');
		$skype = Check::postAbsent('skype');
		$email = Check::postAbsent('email');
		$bp_perfect = User::getByPerfect($perfect);
		if(strlen($perfect) && $bp_perfect && $bp_perfect['id'] != User::$id){
			Out::error('perfect_exists');
		}
		$bp_skype = User::getBySkype($skype);
		if(strlen($skype) && $bp_skype && $bp_skype['id'] != User::$id){
			Out::error('skype_exists');
		}
		$bp_email = User::getByEmail($email);
		if(strlen($email) && $bp_email && $bp_email['id'] != User::$id){
			Out::error('email_exists');
		}
		$skype = Check::postAbsent('skype');
		$password = isset($_POST['password']) ? $_POST['password'] : 0;
		if(!strlen(trim($password))) $password = 0;
		User::update($perfect, $skype, $password, $email);
		Out::ok();
	}
	public function password(){
		$password = Check::postAbsent('password');
		$u = User::getById(User::$id);
		if($u['password'] == User::passwordHash($password))
			Out::ok();
		else
			Out::error();
	}
	public function info(){
		$user_id = Check::postAbsent('user_id');
		$u = User::getById($user_id);
		if(!$u)
			Out::error();
		if(!User::$boss){
			unset($u['hash']);
			unset($u['password']);
			unset($u['perfect']);
		};
		Out::ok($u);
	}
	public function cube_all(){
		$r = Db::q("SELECT * FROM user_banner WHERE user_id != 0");
		if(!User::$logged)
			Out::ok($r);
		foreach($r as &$rr)
			if($rr['user_id'] == User::$id)
				$rr['my'] = true;
		Out::ok($r);
	}
	public function cube_set(){
		if(!User::$logged)
			Out::error();
		if(!User::bannersAvailable())
			Out::error('no_banners_for_you');
		$n = Check::postAbsent('n');
		$i = Check::postAbsent('i');
		$j = Check::postAbsent('j');
		$user_id = User::$id;

		$p_num = User::bannersPnum();
		if($p_num < 0)
			Out::error();

		Db::q("UPDATE user_banner SET user_id=i:user_id, p_num=i:p_num WHERE n=i:n AND j=i:j AND i=i:i", array(
			'user_id' => $user_id,
			'n' => $n,
			'i' => $i,
			'j' => $j,
			'p_num' => $p_num,
		));
		Out::ok();
	}
	public function cube_rest(){
		if(!User::$logged)
			Out::ok(0);
		Out::ok(User::bannersAvailable());
	}
	public function cube_packet_banner(){
		Out::ok(Config::$packetBanner);
	}
	public function cube_user(){
		if(!User::$logged)
			Out::ok(array());
		Out::ok(Db::q("SELECT * FROM user_banner WHERE user_id=i:user_id", array('user_id' => User::$id)));
	}

}

new ActionsUser();