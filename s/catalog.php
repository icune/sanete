<?php
	class Catalog extends GetController{
		public function index(){
			Gui::view('catalog.php', array(
				'cats' => Db::q("SELECT cat as name, COUNT(cat) count FROM product GROUP BY cat")
			));
		}
	}
	
	class PostCatalog extends PostController{
		public function filters(){
			$categories = Db::q("SELECT cat FROM product GROUP BY cat");
			$codes = Db::q("SELECT code FROM product GROUP BY code");
			$names = Db::q("SELECT name FROM product GROUP BY name");
			$price = Db::q("SELECT min(price) min_price, max(price) max_price FROM product");
			$size = Db::q("SELECT min(size_x) min_size_x, max(size_x) max_size_x, min(size_y) min_size_y, max(size_y) max_size_y, min(size_z) min_size_z, max(size_z) max_size_z FROM product");
			$weight = Db::q("SELECT min(weight) min_weight, max(weight) max_weight FROM product");
			$manf = Db::q("SELECT manf FROM product GROUP BY manf");

			Out::ok(array(
				'categories' => Utils::simple($categories),
				'codes' => Utils::simple($codes),
				'names' => Utils::simple($names),
				'price' => $price[0],
				'size' => $size[0],
				'weight' => $weight[0],
				'manf' => Utils::simple($manf)
			));
		}
		public function get(){
			if(!isset($_POST['num']))
				$num = 0;
			else
				$num = intval($_POST['num']);
			$num = $num ? $num : 0;
			$where = $this->filtersWhere();
			$products = Db::q("SELECT * FROM product $where LIMIT ".$num*Config::PER_PAGE.','.Config::PER_PAGE);
			$count = Db::q("SELECT COUNT(*) count FROM product $where");
			$count = $count[0]['count'];
			Out::ok(array(
				'products' => $products,
				'end' => count($products) != Config::PER_PAGE,
				'count' => $count
			));
		}
		private function filtersWhere(){
			if(!isset($_POST['filters']))
				return'';
			$ands = array();
			$filters = $_POST['filters'];
			foreach($filters as $k=>$f){
				if(!isset($f['type']))
					continue;
				if(!isset($f['val']))
					continue;
				if($f['type'] == 'range'){
					$mm = $f['val'];
					if(!isset($mm[0]) || !isset($mm[1]))
						continue;
					$ands []= Db::sp('(c:c >= f:min AND c:c <= f:max)', array('c'=>$k, 'min'=>$mm[0], 'max'=>$mm[1]));
				}
				if($f['type'] == 'or'){
					$mm = $f['val'];
					if(!count($mm))
						continue;
					$ors = array();
					foreach($mm as $m)
						$ors []= Db::sp('c:c = s:s', array('c' => $k, 's'=>$m));
					$ands []= '(' . implode(' OR ', $ors) . ')';
				}
			}
			if(!count($ands))
				return '';
			return 'WHERE '.implode(' AND ', $ands);
		}

		public function update(){
			Check::postAbsent('products');
			if(!User::$logged){
				Out::error();
				return;
			}
			$products = $_POST['products'];
			foreach($products as $k => $p){
				$ups = array();
				foreach($p as $f => $v)
					$ups []= Db::sp('c:c=s:v', array('c' => $f, 'v' => $v));
				if(count($ups)){
					$sql = 'UPDATE product SET '.implode(' , ', $ups) . ' WHERE id=i:id';
					Db::q($sql, array('id' => $k));	
				}
				
			}
			Out::ok();
		}
		public function delete(){
			Check::postAbsent('id');
			if(!User::$logged){
				Out::error();
				return;
			}
			$id = $_POST['id'];
			Db::q('DELETE FROM product WHERE id=i:id', array(
				'id' => $id
			));
			Out::ok();
		}
	}
	$cat = new Catalog();
	if($cat->post)
		new PostCatalog();