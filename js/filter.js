window.Filter = {
	unionSelecter:function(params){
		var cont = params.container;
		var data = params.data;
		var namer = params.namer;
		var currentData = data;
		var selectedData = [];
		var _inp = $('<input type="text" class="filter-input">');
		if(params.placeholder)
			_inp.attr('placeholder', params.placeholder);
		cont.append(_inp);
		cont.append('<div class="not-selected">Не выбрано</div>');
		_inp.autocomplete({
			source:function(inp, cb){
				cb( Data.filter(inp.term) ) ;
			},
			select:function(ev, ui){
				var text = ui.item.label;
				console.log(ui.item);
				Data.add(text);
				addElem(text);
				showAbsence();
				setTimeout(function(){
					_inp.val('');
				}, 20);
			}
		})
		function addElem(text){
			var elemText = namer ? namer(text) : text;
			var elem = $('<div class="union-elem">\
							<span class="union-text"></span>\
							<span class="ui-icon ib ui-icon-circle-close"></span>\
						</div>');
			elem.data('text', text);
			elem.find('.ui-icon').click(function(){
				Data.remove(text);
				elem.remove();
				showAbsence();
			});
			elem.find('.union-text').text(elemText);
			if(namer){
				elem.attr('title', text);
				elem.tooltip({position:{my:"center bottom", at:"center top"}});
			}

			elem.appendTo(cont);
		}

		function showAbsence(){
			if(Data.empty())
				cont.find('.not-selected').show();
			else
				cont.find('.not-selected').hide();
		}

		var Data = {
			add:function(text){
				currentData.splice(currentData.indexOf(text), 1);
				selectedData.push(text);
				if(params.onUpdate instanceof Function) params.onUpdate(params.key, selectedData);
			},remove:function(text){
				selectedData.splice(selectedData.indexOf(text), 1);
				currentData.push(text);
				if(params.onUpdate instanceof Function) params.onUpdate(params.key, selectedData);
			},
			filter:function(term){
				var r = [];
				var inx = 0;
				for(var k in currentData)
					if(currentData[k].toLowerCase().indexOf(term) != -1){
						r.push(currentData[k]);
						if(++inx == 10)
							break;
					}
				return r;
			},
			empty:function(){
				return !selectedData.length;
			}
		}
	},
	toFromSelecter:function(params){
		var cont = params.container;
		var data = params.data;
		var elemTpl = $('<div class="size-to-low">\
							<div class="slider"></div>\
						</div><div class="range-container"><span class="title"></span><span class="numbers">20-50</span>\
						</div>');
		for(var k in data){
			(function(_k, _p){
				var elem = elemTpl.clone().appendTo(cont);
				elem.find('.title').text(_p.label ? _p.label + ': ' : '');	
				elem.find('.numbers').text(_p.min + '' + ' - ' + _p.max + '');
				// elem.find('.slider').slider({
				// 	range: true,
				// 	min: parseFloat(_p.min),
				// 	max: parseFloat(_p.max),
				// 	values: [ _p.min, _p.max ],
				// 	change: function( event, ui ) {
				// 		elem.find( ".numbers" ).text( "" + ui.values[ 0 ] + '' + " - " + ui.values[ 1 ] + '' );
				// 		if(params.onUpdate instanceof Function) 
				// 			params.onUpdate(_k, [ui.values[ 0 ], ui.values[ 1 ]]);
				// 	}
				// });
				noUiSlider.create(
					elem.find('.slider').get(0),
					{
						range : {
							'min' : parseFloat(_p.min),
							'max' : parseFloat(_p.max)
						}, 
						start : [parseFloat(_p.min), parseFloat(_p.max)]
					}
				)
				elem.find('.slider').get(0).noUiSlider.on('change', function(values){
					elem.find( ".numbers" ).text( "" + parseInt(values[ 0 ]) + '' + " - " + parseInt(values[ 1 ]) + '' );
					if(params.onUpdate instanceof Function) 
						params.onUpdate(_k, [values[ 0 ], values[ 1 ]]);
				})
			})(k, data[k]);
			
		}
	},
	initFilters:function(filters, evokeFiltering){
		$('.filter .reset-button').show();
		$('.filter .reset-button').button().click(function(){
			window.location.href = window.location.href;
		});
		// Filter.unionSelecter({
		// 	container:$('#cat-filter'),
		// 	data:filters.categories,
		// 	namer:namer,
		// 	key:'cat',
		// 	onUpdate:function(key, val){
		// 		evokeFiltering('or', key, val)
		// 	}
		// });
		Filter.unionSelecter({
			container:$('#manf-filter'),
			data:filters.manf,
			namer:namer,
			key:'manf',
			onUpdate:function(key, val){
				evokeFiltering('or', key, val)
			}
		});
		Filter.unionSelecter({
			container:$('#codes-filter'),
			data:filters.codes,
			namer:function(x){
				return x;
			},
			key:'code',
			onUpdate:function(key, val){
				evokeFiltering('or', key, val)
			}
		});
		Filter.unionSelecter({
			container:$('#names-filter'),
			data:filters.names,
			namer:namer,
			key:'name',
			onUpdate:function(key, val){
				evokeFiltering('or', key, val)
			}
		});
		Filter.toFromSelecter({
			container:$('#size-filter'),
			data:{
				size_x:{
					label:'Ширина',
					min:filters.size.min_size_x,
					max:filters.size.max_size_x
				},
				size_y:{
					label:'Высота',
					min:filters.size.min_size_y,
					max:filters.size.max_size_y
				},
				size_z:{
					label:'Глубина',
					min:filters.size.min_size_z,
					max:filters.size.max_size_z
				}
			},
			
			onUpdate:function(key, val){
				evokeFiltering('range', key, val)
			}
		});
		Filter.toFromSelecter({
			container:$('#price-filter'),
			data:{
				price:{
					label:'',
					min:filters.price.min_price,
					max:filters.price.max_price
				}
			},
			
			onUpdate:function(key, val){
				evokeFiltering('range', key, val)
			}
		});
		Filter.toFromSelecter({
			container:$('#weight-filter'),
			data:{
				weight:{
					label:'',
					min:filters.weight.min_weight,
					max:filters.weight.max_weight
				}
			},
			
			onUpdate:function(key, val){
				evokeFiltering('range', key, val)
			}
		});
		function namer(x){
			var r = '';
			var parts = x.split(/\s+|\//);
			for(var k in parts){
				r += parts[k][0];
				if(parts[k][parts[k].length-1] == '/')
					r += '/';
			}
			return r;
		}
	},
	loadFilters:function(evokeFiltering){
		var me = this;
		$.post('/catalog', {act:'filters'}, function(r){
			me.initFilters(r.msg, evokeFiltering);
			$('#filter-loader').remove();
		}, 'json');
	}
};