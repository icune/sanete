window.Net = {
	post: function(script, act, data, cb){
		if(!data) data = {};
		data.act = act;
		$.ajax({
			url: '/'+script,
			type: 'post',		
			dataType: 'json',
			data: data,
			success:function(r){
				
				if(r.status == 'ok'){		
					if(cb.success instanceof Function) cb.success(r.msg);
				}else{
					if(cb.error instanceof Function) cb.error(r.msg)
				}
			},
			error:function(r){if(cb.error instanceof Function) cb.error(r.msg)}
		});
	}
};