window.BasketAdmin = {
	data:{
		count:0,
		basket_idsalt:0
	},
	gui:{
		modal:{
			open:function(){
				var modal = $('.modal-basket.template').clone().removeClass('template');
				this.close = Ui.modal(modal).close;
				this.modal = modal;
				modal.find('button.submit').button();
			},
			fill:function(got){
				var me = this;
				var modal = this.modal;
				modal.find('.loader').remove();
				if(got.length)
					modal.find('.blocks').html('');
				else
					modal.find('.blocks').html('Не выбрано ни одного продукта');
				for(var k in got)
					modal.find('.blocks').append( Ui.productBlock(got[k], {forBasket:true}) );
				modal.find('button.submit').click(function(){
					if(!modal.find('.submit-contacts').val().trim()){
						alert('Укажите контактные данные');
						return;
					}
					if(!got.length){
						alert('Нельзя сделать заказ без продуктов');
						return;	
					}
					cb.submit({
						contacts:modal.find('.submit-contacts').val(), 
						comment:modal.find('.submit-comment').val(),
						current:me.grabOrder()
					});
				})
			}

		}
	},
	get:function(cb){
		Net.post('basket', 'get', {basket_idsalt:this.data.basket_idsalt}, {
			success:cb.success,
			error:cb.error
		});
	},
	open:function(basket_id){
		var me = this;
		me.data.basket_idsalt = basket_id;
		me.gui.modal.open();
		this.get({
			success:function(r){
				me.gui.modal.fill(r);
			},
			error:function(){
				Ui.error('Мы не можем открыть корзину( Попробуйте еще.');
				me.gui.modal.close();
			}
		})
	}
}