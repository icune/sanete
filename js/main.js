$(function(){
	toTop();
	$(window).scroll(toTop);
	function toTop(){
		if($(window).scrollTop() > 100)
			$('.to-top').show();
		else
			$('.to-top').hide();
	}
	$('.to-top').click(function(){
		$('html,body').animate({
			'scrollTop':0
		}, 300, function(){
			$('.to-top').hide();		
		})
	});

	
	$('.basket.menu-item').click(function(){
		Basket.open();
	})

	$('#cats').click(function(){
		if(!$('.page-menu').hasClass('opened'))
			$('.page-menu').addClass('opened').removeClass('closed');
		else{
			$('.page-menu').removeClass('opened').addClass('closed');
			filter();
		}
	})
	$('.page-menu .cross,#show').click(function(){
		$('.page-menu').removeClass('opened').addClass('closed');
		filter();
	})
	$('#reset').click(function(){
		$('.page-menu .category').removeClass('active');
		count();
	})
	$('.page-menu .category').click(function(){
		
		$(this).find('.plus').addClass('minus');
		$(this).addClass('active');
		
		count();
	})
	$('.page-menu .category .plus').click(function(e){
		e.stopPropagation();
		$(this).closest('.category').find('.plus').removeClass('minus');
		$(this).closest('.category').removeClass('active');
		count();
	});
	var oldCats = [];
	function filter(){
		var cats = [];
		$('.page-menu .category.active .name').each(function(){
			cats.push($(this).text());
		});
		if(oldCats.join() != cats.join())
			evokeFiltering('or', 'cat', cats);
		oldCats = cats;
	}
	function count(){
		var count = $('.page-menu .category.active').size();
		if(count){ 
			$('#cats .count').text('(' + count + ')');
			$('#show').show();
			$('#reset').show();
		}else{
			$('#cats .count').text('');
			$('#show').hide();
			$('#reset').hide();
		}
	}
})