$(function(){

	function renderProducts(resp){
		for(var k in resp.products){
			var p = resp.products[k];
			var el = Ui.productBlock(p);
			$('.content .blocks').append(el);
		}
		if(resp.end)
			$('#product-loader').hide();
		else
			$('#product-loader').show();
		if(resp.end && !resp.products.length && !Globals.num)
			$('.nothing-found').show();
		else
			$('.nothing-found').hide();
		$('.filter .found').text('Найдено: ' + resp.count);
	}


	Filter.loadFilters(evokeFiltering);


	function loadProducts(cb){
		var data = {act:'get', filters:Globals.filters, num:Globals.num};
		$.post('/catalog', data, function(r){
			for(var k in r.msg.products){
				Globals.products[r.msg.products[k].id] = r.msg.products[k];
			}
			renderProducts(r.msg);
			if(cb instanceof Function) cb();
		}, 'json');
	};
	loadProducts();
	Globals.num = 0;
	
	$(window).on('scroll', windowScroll);
	function windowScroll(){
		if($(window).scrollTop() + $(window).height() > $('#block-end').offset().top){
			$(window).off('scroll', windowScroll);
			setTimeout(function(){
				Globals.num = parseInt(Globals.num)+1;
				loadProducts(function(){
					$(window).on('scroll', windowScroll);
				});
			}, 500);
			
		}
	}

	function evokeFiltering(type, key, val){
		Globals.num = 0;
		Globals.filters[key] = {type:type, val:val};
		$('#product-loader').show();
		$('.block:not(.template)').remove();
		setTimeout(function(){
			loadProducts();
		}, 500);
		
	}
	window.evokeFiltering = evokeFiltering;
})