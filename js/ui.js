window.Ui = {
	error:function(html){
		var el = $('<div class="upper-window">');
		el.appendTo('body');
		el.html(html);
		el.animate({top:0}, 500, function(){
			var remed = false;
			setTimeout(function(){
				if(remed) return;
				el.animate({top:'-100px'}, 500, function(){el.remove()});
			}, 5000);
			el.click(function(){
				remed = true;
				el.animate({top:'-100px'}, 500, function(){el.remove()});
			});
		})
	},
	modal:function(elem){
		var cover = $('<div class="cover">').appendTo('body');
		var modal = elem.addClass('modal-content').appendTo(cover);
		var modalClose = $('<div class="modal-close">Закрыть</div>').appendTo(cover);
		var defCss = {
			'top':'50%', 
			'transform':modal.css('transform'), 
			'-webkit-transform':modal.css('-webkit-transform')
		};

		function posCheck(){
			if(modal.height() > $(window).height()-100){
				modal.css('top', '100px');
				modal.css('transform', 'translateX(-50%)');
				modal.css('-webkit-transform', 'translateX(-50%)');
			}else{
				modal.css(defCss);
			}
			modalClose.css({
				left:modal.position().left + parseInt(modal.css('width')) - modalClose.width(),
				top:modal.position().top - modalClose.height()-5
			})
		}
		function escExit(e){
			if(e.keyCode == 27)
				close();
		}
		posCheck();
		$(window).on('resize', posCheck);
		$(window).on('keyup', escExit);

		modalClose.click(close);
		function close(){
			$(window).off('resize', posCheck);		
			$(window).off('keyup', escExit);
			cover.remove();	
		}

		modal.click(function(e){
			e.stopPropagation();
		})
		cover.click(close);
		return {
			close:close
		};
	},
	productBlock:function(product, opt){
		if(!opt) opt = {};
		var p = product;
		var el = $('.block.template').clone().removeClass('template');
		el.find('.name').text(p.name);
		el.find('.code').text(p.code);
		el.find('.manf').text(p.manf);
		el.find('.cat').text(p.cat);
		el.find('.gost').text(p.gost);
		el.find('.weight').text(p.weight);
		el.find('.price').text(p.price);
		el.find('.pic').attr('src', '/img/img_new/'+p.img).click(function(){
			Ui.modal($('<img src="/img/img_new/'+p.img+'" />'));
		});
		el.find('.size').text(p.size_x+'x'+p.size_y+'x'+p.size_z);

		el.data('product_id', product.id);

		var inp = el.find('.block-inner input').eq(0).spinner({
			numberFormat:'n',
			min:1
		}).keyup(function(e){
			var el = $(this);
			if(!el.val().trim()){
				el.val('1');
				return;
			}
			var mv = '';
			for(var k in el.val()){
				if(el.val()[k].match(/^[0-9]{1}$/))
					mv += el.val()[k];
			}
			if(!mv)
				mv = '1';
			el.val(mv);
		})

		


		if(opt.forBasket){
			el.addClass('for-basket');
			el.find('button.delete-from-basket').show();
			el.find('button.delete-from-basket').button().click(function(){
				el.remove();
			})
			inp.val(product.count);
		}else{
			el.find('button.add-to-basket').show();

			el.find('button.add-to-basket').button().click(function(){
				Basket.add(p.id, inp.spinner('value'));
			});
		};


		return el;
	}
}