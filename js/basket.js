window.Basket = {
	data:{
		count:0
	},
	gui:{
		fixed:{
			_elem:function(){
				return $('.basket.menu-item');
			},
			add:function(new_count){
				var me = this;
				this._elem().find('.count').text(new_count);
				this._elem().css('background-color', 'rgb(120, 237, 135)');
				setTimeout(function(){
					me._elem().removeAttr('style');
				}, 500);
			},
			remove:function(new_count){
				var me = this;
				this._elem().find('.count').text(new_count);
				this._elem().css('background-color', 'rgb(231, 127, 127)');
				setTimeout(function(){
					me._elem().removeAttr('style');
				}, 500);
			}
		},
		modal:{
			open:function(){
				var modal = $('.modal-basket.template').clone().removeClass('template');
				this.close = Ui.modal(modal).close;
				this.modal = modal;
				modal.find('button.submit').button();
			},
			fill:function(got, cb){
				var me = this;
				var modal = this.modal;
				modal.find('.loader').remove();
				if(got.length)
					modal.find('.blocks').html('');
				else
					modal.find('.blocks').html('Не выбрано ни одного продукта');
				for(var k in got)
					modal.find('.blocks').append( Ui.productBlock(got[k], {forBasket:true}) );
				modal.find('button.submit').click(function(){
					if(!modal.find('.submit-contacts').val().trim()){
						alert('Укажите контактные данные');
						return;
					}
					if(!got.length){
						alert('Нельзя сделать заказ без продуктов');
						return;	
					}
					cb.submit({
						contacts:modal.find('.submit-contacts').val(), 
						comment:modal.find('.submit-comment').val(),
						current:me.grabOrder()
					});
				})
			},
			grabOrder:function(){
				var modal = this.modal;
				var blocks = modal.find('.block');
				var r = [];
				blocks.each(function(){
					r.push({
						product_id:$(this).data('product_id'),
						count:$(this).find('input.count').spinner('value')
					})
				})
				return r;
			}

		}
	},
	add:function(product_id, count){
		var me = this;
		this.data.count++;
		this.gui.fixed.add(this.data.count);
		Net.post('basket', 'add', {product_id:product_id, count:count}, {
			error:function(){
				Ui.error('К сожалению мы не смогли добавить продукт <b>'+Globals.products[product_id].name+'</b> в корзину. Попробуйте еще раз.');
				me.gui.fixed.remove(me.data.count-1);
			}
		})
	},
	get:function(cb){
		Net.post('basket', 'get', {}, {
			success:cb.success,
			error:cb.error
		});
	},
	open:function(){
		var me = this;
		me.gui.modal.open();
		this.get({
			success:function(r){
				me.gui.modal.fill(r, {submit:me.submit.bind(me)});
			},
			error:function(){
				Ui.error('Мы не можем открыть корзину( Попробуйте еще.');
				me.gui.modal.close();
			}
		})
	},
	submit:function(p){
		var me = this;
		Net.post('basket', 'order', p, {
			success:function(){
				alert('Ваш заказ принят!');
				me.gui.modal.close();
				window.location.href = window.location.href;
			},
			error:function(){
				me.gui.modal.close();
				Ui.error('К сожалению мы не смогли принять ваш заказ. Попробуйте позже.')	
			}
		});
	}
}