$(function(){
	Filter.loadFilters(evokeFiltering)
	$('#show-changed').button();
	$('#save').button();
	$('#show-filter').button();
	$('#show-filter').change(function(e){
		showFilter($(this).is(':checked'));	
	})
	$('#show-changed').change(function(e){
		showChanged($(this).is(':checked'));	
	})
	function showFilter(show){
		if(show)
			$('.filter').animate({
				maxHeight:200,
				opacity:1,
				padding:13
			}, 500);
		else
			$('.filter').animate({
				maxHeight:0,
				opacity:0,
				padding:0
			}, 500);
	}
	var changedShown  = false;
	function showChanged(show){
		if(!show){
			$('.changed-products').children().remove();
		}else{
			fillChanged();
		};
		changedShown = show;
	}

	function fillChanged(){
		var pc = Globals.product_changed;
		for(var k in pc)
			$('.changed-products').append(product(Globals.products[k]));
	}

	function renderProducts(resp){
		for(var k in resp.products){
			var p = resp.products[k];
			var el = product(p);
			$('.content .products').append(el);
		}
		if(resp.end)
			$('#product-loader').hide();
		else
			$('#product-loader').show();
		if(resp.end && !resp.products.length && !Globals.num)
			$('.nothing-found').show();
		else
			$('.nothing-found').hide();
		$('.filter .found').text('Найдено: ' + resp.count);
	}
	function loadProducts(cb){
		var data = {act:'get', filters:Globals.filters, num:Globals.num};
		$.post('/catalog', data, function(r){
			for(var k in r.msg.products){
				Globals.products[r.msg.products[k].id] = r.msg.products[k];
			}
			renderProducts(r.msg);
			if(cb instanceof Function) cb();
		}, 'json');
	};
	loadProducts();
	Globals.num = 0;
	
	$(window).on('scroll', windowScroll);
	function windowScroll(){
		if($(window).scrollTop() + $(window).height() > $('#block-end').offset().top){
			$(window).off('scroll', windowScroll);
			setTimeout(function(){
				Globals.num = parseInt(Globals.num)+1;
				loadProducts(function(){
					$(window).on('scroll', windowScroll);
				});
			}, 500);
			
		}
	}

	function evokeFiltering(type, key, val){
		Globals.num = 0;
		Globals.filters[key] = {type:type, val:val};
		$('#product-loader').show();
		$('.products .product:not(.template)').remove();
		setTimeout(function(){
			loadProducts();
		}, 500);
		
	}
	window.evokeFiltering = evokeFiltering;
	function _field(_fdesc, _val, _change, _changed){
		if(_fdesc.field == 'gost')
			return null;
		if(_fdesc.field == 'img'){
			var img = $('<img/>');
			img.attr('src', '/img/img_new/'+_val);
			img.click(function(){
				Ui.modal($('<img src="/img/img_new/'+_val+'" />'));
			})
			var dv = $('<div class="im-container">');
			img.appendTo(dv);
			if(_changed && _changed['img'])
				img.addClass('changed');
			var but = $('<button>').addClass('revert').text('Отменить изменения');
			dv.append(but);
			return dv;
		}
		var cbox = _fdesc.type && _fdesc.type.toLowerCase().indexOf('tinyint') != -1;
		var inp = $('<input type="text" class="edit" field="'+_fdesc.field+'">').val(_val);
		if(cbox){
			var inp = $('<input type="checkbox" class="edit" field="'+_fdesc.field+'">');
			if(_val|0 == 1)
				inp.attr('checked', true);
		}
		var el = $("<div class='field'>");
		if(_changed && _changed[_fdesc.field])
				el.addClass('changed');
		var name = $('<span class="name">').text(_fdesc.name ? _fdesc.name : '');
		el.append(name);
		el.append(inp);
		if(_fdesc.name && _fdesc.name.length && _fdesc.name[0] == '\\')
			el.hide();
		if(cbox)
			inp.click(function(){
				change($(this).is(':checked')|0);
			});
		else
			inp.keyup(function(){
				change($(this).val());
			});
		function change(v){
			el.addClass('changed');
			_change({field:_fdesc.field, val:v});

		}
		return el;
	}
	function product(p){
		var el = $('.product.template').clone().removeClass('template');
		el.attr('product_id', p.id);
		var _fn = Globals.product_field;
		function fill(){
			for(var k in _fn){
				var inp = _field(_fn[k], p[k], fieldChange, Globals.product_changed[p.id]);
				if(k == 'img')
					el.prepend(inp);
				else
					el.find('.fields').append(inp);
				
			};
			if(Globals.product_changed[p.id])
				el.find('.revert').show();
			el.find('.revert').click(function(){
				revert();
				delete Globals.product_changed[p.id];
				$('.changed-products .product[product_id='+p.id+']').remove();
				$('.products .product[product_id='+p.id+']').data('revert')();
			});
		}
		fill();
		function revert(){
			console.log('revert');
			el.find(':not(.struct)').remove();
			fill();
		}
		el.data('revert', revert);

		function fieldChange(c){
			if(changedShown){
				showChanged(false);
				$('#show-changed').click();
			}
			el.find('.revert').show();
			if(Globals.product_changed[p.id])
				Globals.product_changed[p.id][c.field] = c.val;
			else{
				Globals.product_changed[p.id] = {};
				Globals.product_changed[p.id][c.field] = c.val;
			}
		}
		el.find('.delete').click(function(){
			if(!confirm('Точно удалить?')) return;
			el.find('.delete').text('...');
			setTimeout(function(){
				Net.post('catalog', 'delete', {id:p.id}, {
					success:function(){
						
						el.remove();
					},
					error:function(){
						Ui.error('Не удалось удалить. Попробуйте еще.');
						el.find('.delete').text('Удалить');
					}
				})
			}, 400);
			
		})
		return el;
	}



	$('#save').click(function(){
		dl = function(o){
			var r = 0;
			for(var k in o)
				r++;
			return r;
		}
		if(!dl(Globals.product_changed)){
			Ui.error('Не изменено ни одного продукта');
			return;
		}

		Net.post('catalog', 'update', {products:Globals.product_changed}, {
			success:function(){
				alert('Сохранено!');
				window.location.href = window.location.href;
			},
			error:function(){
				Ui.error('Не удалось сохраниться. Попробуйте еще.');
			}
		})
	});
})