function Uploader(opt){
	if(!opt) opt = {};
	if(!opt.callbacks) opt.callbacks = {};
	cbs = opt.callbacks;
	var formHtml = '<form enctype="multipart/form-data" style="display: none;" class="upload-form" action="/s/upload.php">\
					<input type="file" name="file" class="img-uploader" class="img-uploader"/>\
					<input type="text" name="act" value="upload" />\
					</form>';
	var form = $(formHtml);
	form.find('.img-uploader').on('change', function(){
		$.ajax({
			url: '/upload',
			type: 'post',		
			dataType: 'json',
			data: new FormData(form.get(0)),
			cache: false,
			contentType: false,
			processData: false,		
			beforeSend: function() {
				if(cbs.selected instanceof Function) cbs.selected();
			},
			complete: function() {
			},	
			success: function(json) {
				if(json['status'] == 'ok'){
					if(cbs.success instanceof Function) cbs.success({file:json.file});
				}else{
					if(cbs.error instanceof Function) cbs.error();
				}
			},			
			error: function(xhr, ajaxOptions, thrownError) {
				if(cbs.error instanceof Function) cbs.error();
			}
		});
	});
	form.find('.img-uploader').trigger('click');

	return form;
}

