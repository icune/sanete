$(function(){
	var clicked = false;
	$('.enter').click(function(){
		if(clicked) return;
		clicked = true;
		var lEl = $('#login');
		var pEl = $('#password');
		if(!lEl.val()){
			blink(lEl, function(){
				clicked = false;
			});
			return;
		}
		if(!pEl.val()){
			blink(pEl, function(){
				clicked = false;
			});
			return;
		}
		Net.post('user', 'login', {login:lEl.val(), password:pEl.val()}, {
			success:function(r){
				setTimeout(function(){
					document.cookie = r + ';path=/;'
					window.location.href = '/';
				}, 1000);
				
			},
			error:function(){
				blink(lEl, function(){clicked = false;});
				blink(pEl, function(){clicked = false;});
			}
		})
	})
	$('.login,.password').keydown(function(e){
		if(e.keyCode == 13)
			$('.enter').click();
	})
	function blink(elem, done){
		var inx = 0;
		var ival = setInterval(timeOut, 300);
		function timeOut(){
			elem.toggleClass('wrong');
			if(inx++ >= 3){
				clearInterval(ival);
				if(done instanceof Function) done();
			}
		}
	}
})

function logout(){
	window.location.href = '/logout';
}